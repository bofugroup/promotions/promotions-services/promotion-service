package com.bofugroup.service.promotion.bussines.dto;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="promotion_tbl")
public class Promotion{
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The Database generated product ID")
	Long id;
	@ApiModelProperty(notes = "The Description of Promotion")
	@Column(name = "description", nullable = false)
	String description;
	@ApiModelProperty(notes = "The Start date of Promotion")
	@Column(name = "start_date", nullable = false)
	Date start_date;
	@ApiModelProperty(notes = "The final date of Promotion")
	@Column(name = "final_date", nullable = false)
	Date final_date;
	@ApiModelProperty(notes = "The terms of Promotion")
	@Column(name = "terms", nullable = false)
	String terms;
	@ApiModelProperty(notes = "The benefit of Promotion (%)")
	@Column(name = "benefit", nullable = false)
	float benefit;
	
	
	public Promotion() {}


	public Promotion(String description, Date start_date, Date final_date, String terms, float benefit) {
		super();
		this.description = description;
		this.start_date = start_date;
		this.final_date = final_date;
		this.terms = terms;
		this.benefit = benefit;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getStart_date() {
		return start_date;
	}


	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}


	public Date getFinal_date() {
		return final_date;
	}


	public void setFinal_date(Date final_date) {
		this.final_date = final_date;
	}


	public String getTerms() {
		return terms;
	}


	public void setTerms(String terms) {
		this.terms = terms;
	}


	public float getBenefit() {
		return benefit;
	}


	public void setBenefit(float benefit) {
		this.benefit = benefit;
	}
	
	
	
	

}
