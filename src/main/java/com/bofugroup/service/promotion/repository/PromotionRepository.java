package com.bofugroup.service.promotion.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bofugroup.service.promotion.bussines.dto.Promotion;

@Repository
public interface PromotionRepository extends CrudRepository<Promotion, Long>
{

}
