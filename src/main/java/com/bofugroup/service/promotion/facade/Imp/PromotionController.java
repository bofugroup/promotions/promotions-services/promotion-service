package com.bofugroup.service.promotion.facade.Imp;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bofugroup.service.promotion.bussines.srv.PromotionService;
import com.bofugroup.service.promotion.facade.IPromotionController;
import com.bofugroup.service.promotion.facade.dto.DTOinPromotion;
import com.bofugroup.service.promotion.facade.dto.DTOoutPromotion;
import com.bofugroup.service.promotion.facade.mapper.IPromotionMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/promotions/v00/promotions")
@RestController
@Api(value = "Promotion Resource", produces = "application/json")
public class PromotionController implements IPromotionController{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PromotionController.class);
	
	@Autowired
	IPromotionMapper mapper;
	
	@Autowired
	PromotionService promotionService;
	
	
	@ApiOperation(value = "View a list of avaliable Promotion", response = ArrayList.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved List"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden "),
			@ApiResponse(code = 404, message = "the resource you were trying to reach is not found")
	})
	@RequestMapping(value="", method=RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<List<DTOoutPromotion>> all()
	{
		List<DTOoutPromotion> promotions = new ArrayList<>();
		LOGGER.info("init return list of Promotion");
		promotions = mapper.mapOutListPromotion(promotionService.all());
		LOGGER.info("return list of Promotion");	
		return new ResponseEntity<List<DTOoutPromotion>>(promotions, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Save a Promotion in BD", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully save in BD"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "Bad Request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden "),
			@ApiResponse(code = 404, message = "the resource you were trying to reach is not found")
	})
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> add(@RequestBody DTOinPromotion dtoInPromotion)
	{
		DTOoutPromotion response = null;
		response = mapper.mapOutPromotion(promotionService.save(mapper.mapInPromotion(dtoInPromotion)));
		if (response != null) 
		{
			URI location = ServletUriComponentsBuilder.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(response.getId())
					.toUri();
			return ResponseEntity.created(location).build();
		}
		else
		{
			LOGGER.error("Has errors Promotion");
			return ResponseEntity.noContent().build();
		}
	}
	
	
	@ApiOperation(value = "Find a Promotion in BD", response = URI.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Promotion Found"),
			@ApiResponse(code = 201, message = "Successfully but not content"),
			@ApiResponse(code = 303, message = "Bad Request"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden "),
			@ApiResponse(code = 404, message = "the resource you were trying to reach is not found")
	})
	@RequestMapping(value = "/{promotionId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<DTOoutPromotion> findByPrmotion(@PathVariable("promotionId")Long promotionId)
	{
		LOGGER.error("Has erros Promotion");
		return new ResponseEntity<DTOoutPromotion>(new DTOoutPromotion(),HttpStatus.OK);
	}

}
