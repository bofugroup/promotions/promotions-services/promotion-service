package com.bofugroup.service.promotion.facade.dto;

import java.sql.Date;

public class DTOoutPromotion {
	
	
	Long id;
	String name;
	Date start_date;
	Date final_date;
	float benefit;
	String terms;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getFinal_date() {
		return final_date;
	}
	public void setFinal_date(Date final_date) {
		this.final_date = final_date;
	}
	public float getBenefit() {
		return benefit;
	}
	public void setBenefit(float benefit) {
		this.benefit = benefit;
	}
	public String getTerms() {
		return terms;
	}
	public void setTerms(String terms) {
		this.terms = terms;
	}
}
