package com.bofugroup.service.promotion.facade.mapper;

import java.util.List;

import com.bofugroup.service.promotion.bussines.dto.Promotion;
import com.bofugroup.service.promotion.facade.dto.DTOinPromotion;
import com.bofugroup.service.promotion.facade.dto.DTOoutPromotion;

public interface IPromotionMapper {
	
	DTOoutPromotion mapOutPromotion(Promotion promotion);
	Promotion mapInPromotion(DTOinPromotion dtoInPromotion);
	List<DTOoutPromotion> mapOutListPromotion(List<Promotion> promotion);
	

}
