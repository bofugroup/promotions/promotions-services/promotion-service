package com.bofugroup.service.promotion.facade.mapper.Imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bofugroup.service.promotion.bussines.dto.Promotion;
import com.bofugroup.service.promotion.facade.dto.DTOinPromotion;
import com.bofugroup.service.promotion.facade.dto.DTOoutPromotion;
import com.bofugroup.service.promotion.facade.mapper.IPromotionMapper;

@Component("PromotionMapper")
public class PromotionMapper implements IPromotionMapper{

	@Override
	public DTOoutPromotion mapOutPromotion(Promotion promotion) {
		DTOoutPromotion response = new DTOoutPromotion();
		if(promotion != null) 
		{
			response.setBenefit(promotion.getBenefit());
			response.setStart_date(promotion.getStart_date());
			response.setFinal_date(promotion.getFinal_date());
			response.setTerms(promotion.getTerms());
		}
		
		return response;
	}

	@Override
	public Promotion mapInPromotion(DTOinPromotion dtoInPromotion) {
		Promotion promotion = new Promotion();
		if(dtoInPromotion != null) 
		{
			promotion.setBenefit(dtoInPromotion.getBenefit());
			promotion.setStart_date(dtoInPromotion.getStart_date());
			promotion.setFinal_date(dtoInPromotion.getFinal_date());
			promotion.setTerms(dtoInPromotion.getTerms());
		}
		return promotion;
	}

	@Override
	public List<DTOoutPromotion> mapOutListPromotion(List<Promotion> promotion)
	{
		
		List<DTOoutPromotion> response = new ArrayList<DTOoutPromotion>();
		
		if(promotion != null && !promotion.isEmpty()) 
		{
			for (Promotion temp : promotion) 
			{	
				response.add(mapOutPromotion(temp));
			}
		}
		return response;
	}

}
